WordPic

This is a game for hollywood lovers designed using nodejs and mongodb. The game is hosted in heroku under the domain name:
		https://ctfbackend.herokuapp.com
(currently down for financial reasons)

Once you register yourself and log in into the website, you can click the trial run to get used to the game and start playing.

Currently, there are only 5 questions in the database. However questions can be added using the route /addques if you are logged in as admin.

The database is hosted in mlab.

Don't use the admin account for testing. Kindly create your own account.

The website is developed using Node.JS and Express JS