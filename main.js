var express = require('express');
var session = require('express-session');
var swig = require('swig');
var bodyParser = require('body-parser');
var bcrypt = require('bcrypt');
var MongoClient = require('mongodb').MongoClient;
var grid = require("gridfs-stream");
var multer = require('multer');
var fs = require('fs');
var helmet = require('helmet');
var url = "mongodb://cibi:cibicool16@ds259210.mlab.com:59210/WordPic";
//var url="mongodb://localhost:27017/WordPic"
var app = express();

var name="Sign In",link = "/signin";

const no_of_rounds = 4; 
const port = process.env.PORT || 3000;
const upload = multer({dest : 'upload/'});

app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');
app.use(session({secret: 'hattori',name: 'diegocookie', secure: true,
    httpOnly: true,resave: true,
    saveUninitialized: true}));
app.use(bodyParser.json());
app.use(helmet());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(__dirname + "/views"));

function isLoggedIn(req){
	var sess = req.session;
	return sess.user;
}

function formatt(str){
	str = str.toLowerCase();
	str = str.replace("the ","");
	str = str.replace(/[^A-Z0-9]/ig, "");
	return str;
}

function Hash(pass){
	var salt = bcrypt.genSaltSync(no_of_rounds);
	var hash = bcrypt.hashSync(pass,salt);
	return hash;
}

app.get('/forgotpass',function(req,res){

	res.render("forgotpass.html",{"name":name,"link":link,"message":"enter the username and email-id along with the new password"});

});

app.post('/forgotpass',function(req,res){

		var uname = req.body.username;
		var password = req.body.password;
		var hashpass = Hash(password);
		var email = req.body.email;		
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else if(!(uname && password && email))
		{
			res.redirect("/");return;
		}
		else
		{
		var dbo = db.db('WordPic');
		var users = dbo.collection("users");
		users.find({"uname":uname}).toArray(function(err,result){
			if(err)
							{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
			else
			{
			if(result.length == 0 || result.length > 1)
			{
				res.render("message.html",{"message":"The username is not registered. Kindly Register"});
			}
			else
			{
				if(result[0].email != email)
				{
					res.render("message.html",{"message":"The username and email does not match."});
				}
				else
				{
					var newval = {$set : {"uname":uname,"password":hashpass,"email":email,"level":0,"points":0}};
					users.updateOne({"uname":uname,"email":email},newval, function(err, resu) {
						//console.log(resu);
						//console.log(err);
						if(err)
							res.render("message.html",{"message":"some internal error occured. Try again"});
						else
						res.render("signin.html",{"name":name,"link":link});
					});
				}
					
			}

}		});

}  	});



});
app.get('/signin',function(req,res){

	res.render("signin.html",{"name":name,"link":link});


});

app.get('/signup',function(req,res){

		res.render("signup.html",{"name":name,"link":link});

});

app.post('/signup',function(req,res){
	var uname = req.body.username;
		var password = req.body.password;
		var hashpass = Hash(password);
		var email = req.body.email;		
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else if(!(uname && password && email))
		{
			res.redirect("/");return;
		}
		else
		{
		var dbo = db.db('WordPic');
		var users = dbo.collection("users");
		users.find({"uname":uname}).toArray(function(err,result){
			if(err)
							{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
			else
			{
			if(result.length == 0)
			{
				users.insertOne({"uname":uname,"password":hashpass,"email":email,"level":0,"points":0});
				res.render("signin.html",{"name":name,"link":link});
			}
			else
				res.render("signup.html",{message:"user already registered","name":name,"link":link});

}		});

}  	});
});


app.post('/signin',function(req,res){
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var users = dbo.collection("users");
		var uname = req.body.username;
		var password = req.body.password;
		if(!(uname && password))
		{
			res.redirect("/");return;
		}
		else
		{
		users.find({"uname":uname}).toArray(function(err,result){
				if(result.length ==  0)
				{
				res.render("signin.html",{message:"invalid username","name":name,"link":link});
				return;
				}
				if(bcrypt.compareSync(password,result[0].password))
					{
						sess = req.session;
						sess.user = result[0];
						res.redirect('/');
					}
				else
					res.render("signin.html",{message:"wrong password","name":name,"link":link,"name":name,"link":link});

		});

}	}	});
});

app.get('/', function(req, res){
	
	if(isLoggedIn(req))
	{
		var sess = req.session;
		name = sess.user.uname;
		link = "/users?name=" + name;
	}
   res.render("home.html",{"name":name,"link":link});
});

app.get('/addques',function(req,res){
	if(isLoggedIn(req) &&  req.session.user.uname == "joan")
	res.render("addques.html");
	else
				res.render("message.html",{message:"not authenticated","name":name,"link":link});
});

app.post('/addques',upload.single('pic'),function(req,res){
	  	var word = req.body.word;
  	var answer = req.body.answer;
  	var color = req.body.color;
  				var file = req.file;
  		if(!(word && answer && color && file))
  		{
  			res.redirect("/");return;
  		}
  		else
  		{
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var ques = dbo.collection("questions");
		ques.find().toArray(function(err,result){
		console.log(file);
		var gr = grid(dbo,require("mongodb"));
		var writestream = gr.createWriteStream({filename : "imagei"});
		fs.createReadStream(file.path).pipe(writestream);
		writestream.on('close', function (fil) {
  // do something with `file`
  	var qid = result.length;
  	ques.insertOne({"qid":qid,"word":word,"file":fil._id,"answer":answer,"color":color});
  	  	fs.unlink(file.path,function(){
		});
  	res.render("message.html",{"message":"successfully added","name":name,"link":link});
		
  	});
});

}	});

	}	});


app.get('/users', function(req, res){
	
	var iname= req.query.name;
	if(!isLoggedIn(req))
	{
					console.log(name);
					res.render("message.html",{message:"u must be logged in","name":name,"link":link});
					return;
	}
	else if(!(name))
	{
		res.redirect("/");
		return;
	}
	else
	{
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var users = dbo.collection("users");
		users.find({"uname":iname},{ _id: 0, uname: 1, email: 1 , password : 0 , points : 1 , level : 1 }).toArray(function(err,result){
			if(result.length == 0)
				{
				res.render("message.html",{message:"invalid username","name":name,"link":link});
				  return;
				}
				else
				{
				var text = result[0];
			res.render("userprofile.html",{message: text,"name":name,"link":link});
	}	});

}	 });
}  });

app.get("/logout",function(req,res){
	var sess = req.session;
	sess.user = undefined;
	name="Sign In";
	link = "/signin";
	res.redirect("/");
})


app.get('/img',function(req,res){
	if(!(req.query.qid && isLoggedIn(req)))
	{
		res.redirect("/");
		return;
	}
	else
	{
	var qid= parseInt(req.query.qid,10);
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var question = dbo.collection("questions");
		question.find({"qid":qid}).toArray(function(err,result){
			if(result.length == 0)
				{
				  res.send("invalid");
				  return;
				}
			else
			{
			var iid = result[0].file;
			console.log(result[0].qid);
			var gr = grid(dbo,require("mongodb"));
			var readstream = gr.createReadStream({_id : iid});
			readstream.pipe(res);
			readstream.on("close",function(){
				res.send();return;
			});
	}	});
}	});
}	});

app.get('/play',function(req,res){
	if(!isLoggedIn(req))
	{
			res.render("message.html",{message:"u must be logged in","name":name,"link":link});
		return;
	}
	else
	{
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var question = dbo.collection("questions");
		var sess = req.session;
		var user = sess.user;
		question.find({"qid":user.level + 1}).toArray(function(err,result){
			if(result.length == 0)
				{
				res.render("message.html",{message:"WE are out of questions for now","name":name,"link":link});
				  return;
				}
				else
			res.render("question.html",{"name":name,"link":link,"text":result[0].word,"qid":user.level + 1,"color":result[0].color,"level":user.level});
		});
}	});
}
});

app.get("/checkans",function(req,res){
	var ans = req.query.ans;
	var qidstr = req.query.qid;
	if(!isLoggedIn(req))
	{
			res.render("message.html",{message:"u must be logged in","name":name,"link":link});
		return;
	}
	else if(!(ans && qidstr))
	{
		res.redirect("/");
		return;
	}
	else
	{
	var sess = req.session;
	var u = sess.user;
	var qid = parseInt(qidstr,10);
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var question = dbo.collection("questions");
		question.find({"qid":qid}).toArray(function(err,result){
			if(result.length == 0)
			{
				res.render("message.html",{message:"something fishy","name":name,"link":link});
				return;
			}
			var crctans = result[0].answer;
			crctans = formatt(crctans);
			ans = formatt(ans);
			if(ans == crctans)
			{
					if(qid != 0)
					{
						users = dbo.collection("users");
						if(u.level < qid)
						{	
							u.level++;
							u.points++; 
						}
						else
						{
							res.render("question.html",{"name":name,"link":link,"text":result[0].word,"qid":qid,"color":result[0].color,"message":"you have already tried it. So no points","level":u.level});
							return;						
						}
						console.log(u._id);
						var myquery = { uname : u.uname };
  var newvalues = { $set: { level: qid , points : u.points } };
  users.updateOne(myquery, newvalues, function(err, resu) {
  					console.log(resu);
					res.render("question.html",{"name":name,"link":link,"color":result[0].color,"text":result[0].word,"qid":qid,"message":"you are correct! :)","level":u.level});
				});
					}
					else
					res.render("question.html",{"name":name,"link":link,"color":result[0].color,"text":result[0].word,"qid":qid,"message":"you are correct! :)","level":u.level});
					
			}
			else
			{
					res.render("question.html",{"name":name,"link":link,"color":result[0].color,"text":result[0].word,"qid":qid,"message":"wrong answer :(","level":u.level});	
			}
		});
}	});

}	});

app.get("/showans",function(req,res){
	var sess = req.session;
	var u = sess.user;
	if(!isLoggedIn(req))
	{
			res.render("message.html",{message:"u must be logged in","name":name,"link":link});
		return;
	}
	else if(!(req.query.qid))
	{
		res.redirect("/");
		return;
	}
	else
	{
	var qid = parseInt(req.query.qid);
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var question = dbo.collection("questions");
		question.find({"qid":qid}).toArray(function(err,result){
			if(result.length == 0)
			{
			res.render("message.html",{message:"some internal error","name":name,"link":link});
				return;
			}
			else
			{
			var crctans = result[0].answer;
			crctans = crctans.toLowerCase();
			if(qid != 0)
			{
				users = dbo.collection("users");
				if(u.level < qid)
				u.level++;
				console.log(u._id);
				var myquery = { uname : u.uname };
  				var newvalues = { $set: { level: qid } };
  				users.updateOne(myquery, newvalues, function(err, resu) {
  					console.log(u.level);
					res.render("question.html",{"name":name,"link":link,"text":result[0].word,"color":result[0].color,"qid":qid,"message":"the answer is " + crctans,"level":u.level});
				});
			}
			else
					res.render("question.html",{"name":name,"link":link,"text":result[0].word,"color":result[0].color,"qid":qid,"message":"the answer is " + crctans,"level":u.level});
					
	}	});
}	});

}	});


app.get("/leaderboard",function(req,res){
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var users = dbo.collection("users");
		users.find({},{ _id: 0, uname: 1, email: 0 , password : 0 , points : 1 , level : 1 }).toArray(function(err,result){
				  			result = result.sort(function (a,b) {
				  					return ( a.points > b.points ) ? -1 : ((a.points < b.points) ? 1 : ((a.level < b.level) ? -1 : ((a.level > b.level) ? 1 : 0 ) ) );
				  			});
			res.render("leaderboard.html",{"name":name,"link":link,"users": result});return;
		});
}	});
});




app.get('/trial',function(req,res){
	if(!isLoggedIn(req))
	{
		res.send("u must be logged in");return;
	}
	else
	{
	MongoClient.connect(url,function(err,db){
		if(err)
						{console.log(err);res.render("message.html",{"message":"some internal error. kindly refresh"});}
		else
		{
		var dbo = db.db('WordPic');
		var question = dbo.collection("questions");
		question.find({"qid":0}).toArray(function(err,result){
			if(result.length == 0)
				{
			res.render("message.html",{message:"some internal error","name":name,"link":link});
				  return;
				}
				else
			res.render("question.html",{"name":name,"link":link,"text":result[0].word,"qid":0,"color":"#000000"});
		});
}	});
}	});

app.get('*', function(req, res){
  		res.render("message.html",{message:"oops struck somewhere??","name":name,"link":link});
});


app.listen(port,function(error){
	console.log("The server is started" + port);
});